Posgres setup:

Run PgAdmin
As a superuser; from within pgAdmin right click on your database's Extensions and select New Extension.
Then in the drop down associated with Name select the postgis* extensions needed.

[Imgur](https://i.imgur.com/rX6yb07.png)
[Imgur](https://i.imgur.com/4PJagw0.png)

Links:
https://stackoverflow.com/questions/24981784/how-do-i-add-postgis-to-postgresql-pgadmin