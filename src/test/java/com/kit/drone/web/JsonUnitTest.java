package com.kit.drone.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kit.drone.resource.RouteResource;
import com.kit.drone.resource.RouteResource.PointResource;
import com.kit.drone.resource.RouteResource.PointResource.PointResourceBuilder;
import com.kit.drone.resource.RouteResource.PolygonResource;
import com.kit.drone.resource.RouteResource.PolygonResource.PolygonResourceBuilder;
import com.kit.drone.resource.RouteResource.RouteResourceBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

@Slf4j
public class JsonUnitTest {

    @Test
    public void shouldCreateRootJson() throws IOException {
        // given
        ObjectMapper objectMapper = new ObjectMapper();
        RouteResource routeResource = aRoute()
                .name("firstRoute")
                .line(asList(
                        aPoint().x(0).y(0).z(0).build(),
                        aPoint().x(1.2).y(2).z(3).build(),
                        aPoint().x(2.5).y(3).z(3).build(),
                        aPoint().x(5.5).y(5).z(3).build(),
                        aPoint().x(2.3).y(8).z(0).build()))
                .polygons(asList(
                        aPolygon().points(asList(
                                aPoint().x(0).y(0).z(0).build(),
                                aPoint().x(2).y(0).z(0).build(), // subject to change
                                aPoint().x(2).y(2).z(0).build(), // subject to change
                                aPoint().x(0).y(2).z(0).build(), // subject to change
                                aPoint().x(3).y(0).z(2).build(),
                                aPoint().x(3).y(0).z(4).build(),
                                aPoint().x(1).y(0).z(4).build(),
                                aPoint().x(1).y(0).z(2).build(),
                                aPoint().x(0).y(0).z(0).build()
                        )).build(),
                        aPolygon().points(asList(
                                aPoint().x(1).y(0).z(2).build(),
                                aPoint().x(1).y(0).z(4).build(),
                                aPoint().x(3).y(0).z(4).build(),
                                aPoint().x(3).y(0).z(2).build(),
                                aPoint().x(5).y(3).z(2).build(),
                                aPoint().x(5).y(3).z(4).build(),
                                aPoint().x(2).y(3).z(4).build(),
                                aPoint().x(2).y(3).z(2).build(),
                                aPoint().x(1).y(0).z(2).build()))
                                .build(),
                        aPolygon().points(asList(
                                aPoint().x(2).y(3).z(2).build(),
                                aPoint().x(2).y(3).z(4).build(),
                                aPoint().x(5).y(3).z(4).build(),
                                aPoint().x(5).y(3).z(2).build(),
                                aPoint().x(7).y(5).z(2).build(),
                                aPoint().x(7).y(5).z(4).build(),
                                aPoint().x(5).y(5).z(4).build(),
                                aPoint().x(5).y(5).z(2).build(),
                                aPoint().x(2).y(3).z(2).build()))
                                .build(),
                        aPolygon().points(asList(
                                aPoint().x(5).y(5).z(2).build(),
                                aPoint().x(5).y(5).z(4).build(),
                                aPoint().x(7).y(5).z(4).build(),
                                aPoint().x(7).y(5).z(2).build(),
                                aPoint().x(3.5).y(7).z(0).build(), // subject to change
                                aPoint().x(3.5).y(9).z(0).build(), // subject to change
                                aPoint().x(2).y(9).z(0).build(), // subject to change
                                aPoint().x(2).y(7).z(0).build(), // subject to change
                                aPoint().x(5).y(5).z(2).build()))
                                .build()
                        ))
                .build();

        // when
        String result = objectMapper.writeValueAsString(routeResource);

        // then
        log.info(result);
    }

    @Test
    public void shouldCreateJsonDoubleList() throws JsonProcessingException {
        // given
        ObjectMapper objectMapper = new ObjectMapper();
        List<Double> coordinates = new ArrayList<>();
        coordinates.add(2d);
        coordinates.add(3d);
        coordinates.add(4d);

        // when
        String result = objectMapper.writeValueAsString(coordinates);

        // then
        log.debug(result);
    }

    private RouteResourceBuilder aRoute() {
        return RouteResource.builder();
    }

    private PolygonResourceBuilder aPolygon() {
        return PolygonResource.builder();
    }

    private PointResourceBuilder aPoint() {
        return PointResource.builder();
    }

}
