package com.kit.drone.web;

import com.kit.drone.converter.RouteConverter;
import com.kit.drone.model.Route;
import com.kit.drone.resource.RouteResource;
import com.vividsolutions.jts.geom.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class RouteConverterUnitTest {

    private GeometryFactory geometryFactory = new GeometryFactory();

    @Test
    public void shouldConvert() {
        // given
        RouteConverter converter = new RouteConverter();
        RouteResource routeResource = aRoute()
                .name("firstRoute")
                .line(asList(
                        aPoint().x(1).y(2).z(3).build(),
                        aPoint().x(2).y(3).z(3).build()))
                .polygons(asList(
                        aPolygon().points(asList(
                                aPoint().x(1).y(0).z(2).build(),
                                aPoint().x(1).y(0).z(4).build(),
                                aPoint().x(3).y(0).z(4).build(),
                                aPoint().x(3).y(0).z(2).build(),
                                aPoint().x(5).y(4).z(2).build(),
                                aPoint().x(5).y(3).z(4).build(),
                                aPoint().x(2).y(3).z(4).build(),
                                aPoint().x(2).y(3).z(2).build(),
                                aPoint().x(1).y(0).z(2).build()))
                                .build()))
                .build();
        Route expected = Route.builder().name("firstRoute").path(createLine()).corridor(createMultiPolygon()).build();

        // when
        Route result = converter.convert(routeResource);

        // then
        assertThat(result).isEqualTo(expected);

    }

    private RouteResource.RouteResourceBuilder aRoute() {
        return RouteResource.builder();
    }

    private RouteResource.PolygonResource.PolygonResourceBuilder aPolygon() {
        return RouteResource.PolygonResource.builder();
    }

    private RouteResource.PointResource.PointResourceBuilder aPoint() {
        return RouteResource.PointResource.builder();
    }

    private LineString createLine(){
        return geometryFactory.createLineString(
                new Coordinate[]{
                        new Coordinate(1, 2, 3),
                        new Coordinate(2, 3, 3),
                }
        );
    }

    private Polygon createPolygon() {
        return geometryFactory.createPolygon(
                new Coordinate[]{
                        new Coordinate(1, 0, 2),
                        new Coordinate(1, 0, 4),
                        new Coordinate(3, 0, 4),
                        new Coordinate(3, 0, 2),
                        new Coordinate(5, 4, 2),
                        new Coordinate(5, 3, 4),
                        new Coordinate(2, 3, 4),
                        new Coordinate(2, 3, 2),
                        new Coordinate(1, 0, 2)});
    }

    private Polygon[] createPolygonMassive() {
        List<Polygon> polygonCollection = new ArrayList<>();
        polygonCollection.add(createPolygon());
        return polygonCollection.toArray(new Polygon[1]);
    }

    private MultiPolygon createMultiPolygon(){
        return geometryFactory.createMultiPolygon(createPolygonMassive());
    }
}