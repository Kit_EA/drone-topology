package com.kit.drone.geo;

import com.kit.drone.converter.RouteConverter;
import com.kit.drone.model.Route;
import com.kit.drone.resource.RouteResource;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import org.junit.Test;

import static com.kit.drone.geo.DroneStatus.*;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class GeometryServiceUnitTest {

    private GeometryFactory geometryFactory = new GeometryFactory();
    private GeometryService geometryService = new GeometryService();
    private RouteConverter routeConverter = new RouteConverter();

    @Test
    public void shouldReturnOutOfRouteNorthStatusByTopologyCheckForNorthDeviation() {
        // given
        Point droneLocation = geometryFactory.createPoint(new Coordinate(2, 3, 5));
        Route route = Route.builder()
                .name("firstRoute")
                .path(getPath())
                .corridor(getCorridor())
                .build();


        // when
        CheckResult checkedResultDist = geometryService.checkDronePositionRelatingToRoute(droneLocation, route);

        // then
        assertThat(checkedResultDist.getDroneStatus()).isEqualTo(OUT_OF_ROUTE_NORTH);
    }

    @Test
    public void shouldReturnOutOfRouteSouthStatusByTopologyCheckForSouthDeviation() {
        // given
        Point droneLocation = geometryFactory.createPoint(new Coordinate(2, 3, 1));
        Route route = Route.builder()
                .name("firstRoute")
                .path(getPath())
                .corridor(getCorridor())
                .build();

        // when
        CheckResult checkedResultDist = geometryService.checkDronePositionRelatingToRoute(droneLocation, route);

        // then
        assertThat(checkedResultDist.getDroneStatus()).isEqualTo(OUT_OF_ROUTE_SOUTH);
    }

    @Test
    public void shouldReturnOutOfRouteWestStatusByTopologyCheckForWestDeviation() {
        // given
        Point droneLocation = geometryFactory.createPoint(new Coordinate(0.5, 3, 3));
        Route route = Route.builder()
                .name("firstRoute")
                .path(getPath())
                .corridor(getCorridor())
                .build();


        // when
        CheckResult checkedResultDist = geometryService.checkDronePositionRelatingToRoute(droneLocation, route);

        // then
        assertThat(checkedResultDist.getDroneStatus()).isEqualTo(OUT_OF_ROUTE_WEST);
    }

    @Test
    public void shouldReturnOutOfRouteEastStatusByTopologyCheckForEastDeviation() {
        // given
        Point droneLocation = geometryFactory.createPoint(new Coordinate(6, 3, 3));
        Route route = Route.builder()
                .name("firstRoute")
                .path(getPath())
                .corridor(getCorridor())
                .build();

        // when
        CheckResult checkedResultDist = geometryService.checkDronePositionRelatingToRoute(droneLocation, route);

        // then
        assertThat(checkedResultDist.getDroneStatus()).isEqualTo(OUT_OF_ROUTE_EAST);
    }

    @Test
    public void shouldReturnOnRouteStatusByTopologyCheckOnRouteWhenPathCloseToBorder() {
        // given
        Point droneLocation = geometryFactory.createPoint(new Coordinate(2, 3, 2));
        Route route = Route.builder()
                .name("firstRoute")
                .path(geometryFactory.createLineString(new Coordinate[]{
                        new Coordinate(0.76, 2, 3),
                        new Coordinate(3, 3, 3)}))
                .corridor(getCorridor()).build();

        // when
        CheckResult checkedResultDist = geometryService.checkDronePositionRelatingToRoute(droneLocation, route);

        // then
        assertThat(checkedResultDist.getDroneStatus()).isEqualTo(ON_ROUTE);
    }

    @Test
    public void shouldReturnOutOfRouteSouthStatusByTopologyCheckOnRouteWhenPathMuchAwayFromBorder() {
        // given
        Point droneLocation = geometryFactory.createPoint(new Coordinate(2, 3, 2));
        Route route = Route.builder()
                .name("firstRoute")
                .path(getPath())
                .corridor(getCorridor())
                .build();

        // when
        CheckResult checkedResultDist = geometryService.checkDronePositionRelatingToRoute(droneLocation, route);

        // then
        assertThat(checkedResultDist.getDroneStatus()).isEqualTo(OUT_OF_ROUTE_SOUTH);
    }

    @Test
    public void shouldReturnOutOfRadarsStatusIfDroneIsFarAwayFromCorridor() {
        // given
        Point droneLocation = geometryFactory.createPoint(new Coordinate(10, 3, 2));
        Route route = Route.builder()
                .name("firstRoute")
                .path(getPath())
                .corridor(getCorridor())
                .build();

        // when
        CheckResult checkedResultDist = geometryService.checkDronePositionRelatingToRoute(droneLocation, route);

        // then
        assertThat(checkedResultDist.getDroneStatus()).isEqualTo(OUT_OF_RADARS);
    }

    @Test
    public void shouldReturnFirstSegmentOfCorridorIfDroneLocationBelongsToTakeOff(){
        // given
        Point droneLocation = geometryFactory.createPoint(new Coordinate(0, 0, 0));
        Route route = getRealRoute();

        // when
        boolean isDroneLocationBelongsToTakeOff = geometryService.isDroneLocationBelongsToTakeOff(droneLocation, route);

        // then
        assertThat(isDroneLocationBelongsToTakeOff).isEqualTo(true);

    }

    @Test
    public void shouldReturnLastSegmentOfCorridorIfDroneLocationBelongsToLanding(){
        // given
        Point droneLocation = geometryFactory.createPoint(new Coordinate(3, 8, 0));
        Route route = getRealRoute();

        // when
        boolean isDroneLocationBelongsToLanding = geometryService.isDroneLocationBelongsToLanding(droneLocation, route);

        // then
        assertThat(isDroneLocationBelongsToLanding).isEqualTo(true);

    }

    private Route getRealRoute(){
        RouteResource routeResource = aRoute()
                .name("firstRoute")
                .line(asList(
                        aPoint().x(0).y(0).z(0).build(),
                        aPoint().x(1.2).y(2).z(3).build(),
                        aPoint().x(2.5).y(3).z(3).build(),
                        aPoint().x(5.5).y(5).z(3).build(),
                        aPoint().x(2.3).y(8).z(0).build()))
                .polygons(asList(
                        aPolygon().points(asList(
                                aPoint().x(0).y(0).z(0).build(),
                                aPoint().x(2).y(0).z(0).build(), // subject to change
                                aPoint().x(2).y(2).z(0).build(), // subject to change
                                aPoint().x(0).y(2).z(0).build(), // subject to change
                                aPoint().x(3).y(0).z(2).build(),
                                aPoint().x(3).y(0).z(4).build(),
                                aPoint().x(1).y(0).z(4).build(),
                                aPoint().x(1).y(0).z(2).build(),
                                aPoint().x(0).y(0).z(0).build()
                        )).build(),
                        aPolygon().points(asList(
                                aPoint().x(1).y(0).z(2).build(),
                                aPoint().x(1).y(0).z(4).build(),
                                aPoint().x(3).y(0).z(4).build(),
                                aPoint().x(3).y(0).z(2).build(),
                                aPoint().x(5).y(3).z(2).build(),
                                aPoint().x(5).y(3).z(4).build(),
                                aPoint().x(2).y(3).z(4).build(),
                                aPoint().x(2).y(3).z(2).build(),
                                aPoint().x(1).y(0).z(2).build()))
                                .build(),
                        aPolygon().points(asList(
                                aPoint().x(2).y(3).z(2).build(),
                                aPoint().x(2).y(3).z(4).build(),
                                aPoint().x(5).y(3).z(4).build(),
                                aPoint().x(5).y(3).z(2).build(),
                                aPoint().x(7).y(5).z(2).build(),
                                aPoint().x(7).y(5).z(4).build(),
                                aPoint().x(5).y(5).z(4).build(),
                                aPoint().x(5).y(5).z(2).build(),
                                aPoint().x(2).y(3).z(2).build()))
                                .build(),
                        aPolygon().points(asList(
                                aPoint().x(5).y(5).z(2).build(),
                                aPoint().x(5).y(5).z(4).build(),
                                aPoint().x(7).y(5).z(4).build(),
                                aPoint().x(7).y(5).z(2).build(),
                                aPoint().x(3.5).y(7).z(0).build(), // subject to change
                                aPoint().x(3.5).y(9).z(0).build(), // subject to change
                                aPoint().x(2).y(9).z(0).build(), // subject to change
                                aPoint().x(2).y(7).z(0).build(), // subject to change
                                aPoint().x(5).y(5).z(2).build()))
                                .build()
                ))
                .build();

        return routeConverter.convert(routeResource);
    }


    private RouteResource.RouteResourceBuilder aRoute() {
        return RouteResource.builder();
    }

    private RouteResource.PolygonResource.PolygonResourceBuilder aPolygon() {
        return RouteResource.PolygonResource.builder();
    }

    private RouteResource.PointResource.PointResourceBuilder aPoint() {
        return RouteResource.PointResource.builder();
    }

    private LineString getPath() {
        return geometryFactory.createLineString(new Coordinate[]{
                new Coordinate(2, 2, 3),
                new Coordinate(3, 3, 3),
        });
    }

    private MultiPolygon getCorridor() {
        return geometryFactory.createMultiPolygon(new Polygon[]{
                geometryFactory.createPolygon(new Coordinate[]{
                        new Coordinate(1, 0, 2), new Coordinate(1, 0, 4),
                        new Coordinate(3, 0, 4), new Coordinate(3, 0, 2),
                        new Coordinate(5, 3, 2), new Coordinate(5, 3, 4),
                        new Coordinate(2, 3, 4), new Coordinate(2, 3, 2),
                        new Coordinate(1, 0, 2)
                })
        });
    }
}
