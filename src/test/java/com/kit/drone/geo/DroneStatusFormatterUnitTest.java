package com.kit.drone.geo;

import org.junit.Test;

import static com.kit.drone.geo.DroneStatus.ON_ROUTE;
import static com.kit.drone.geo.DroneStatus.OUT_OF_RADARS;
import static com.kit.drone.geo.DroneStatus.OUT_OF_ROUTE_EAST;
import static com.kit.drone.geo.DroneStatus.OUT_OF_ROUTE_NORTH;
import static com.kit.drone.geo.DroneStatus.OUT_OF_ROUTE_SOUTH;
import static com.kit.drone.geo.DroneStatus.OUT_OF_ROUTE_WEST;
import static org.assertj.core.api.Assertions.assertThat;

public class DroneStatusFormatterUnitTest {

    @Test
    public void shouldReturnFormatMessageForOutOfRouteWest() {
        // given
        DroneStatusFormatter droneStatusFormatter = new DroneStatusFormatter();
        String droneId = "anyDroneId";
        CheckResult checkResult = new CheckResult(100D, OUT_OF_ROUTE_WEST);

        // when
        String message = droneStatusFormatter.formatMessage(droneId, checkResult);

        // then
        assertThat(message).isEqualTo("Drone anyDroneId on the west side from route, distance from route:100.0");
    }

    @Test
    public void shouldReturnFormatMessageForOutOfRouteEast() {
        // given
        DroneStatusFormatter droneStatusFormatter = new DroneStatusFormatter();
        String droneId = "anyDroneId";
        CheckResult checkResult = new CheckResult(100D, OUT_OF_ROUTE_EAST);

        // when
        String message = droneStatusFormatter.formatMessage(droneId, checkResult);

        // then
        assertThat(message).isEqualTo("Drone anyDroneId on the east side from route, distance from route:100.0");
    }

    @Test
    public void shouldReturnFormatMessageForOutOfRouteNorth() {
        // given
        DroneStatusFormatter droneStatusFormatter = new DroneStatusFormatter();
        String droneId = "anyDroneId";
        CheckResult checkResult = new CheckResult(100D, OUT_OF_ROUTE_NORTH);

        // when
        String message = droneStatusFormatter.formatMessage(droneId, checkResult);

        // then
        assertThat(message).isEqualTo("Drone anyDroneId on the north side from route, distance from route:100.0");
    }

    @Test
    public void shouldReturnFormatMessageForOutOfRouteSouth() {
        // given
        DroneStatusFormatter droneStatusFormatter = new DroneStatusFormatter();
        String droneId = "anyDroneId";
        CheckResult checkResult = new CheckResult(100D, OUT_OF_ROUTE_SOUTH);

        // when
        String message = droneStatusFormatter.formatMessage(droneId, checkResult);

        // then
        assertThat(message).isEqualTo("Drone anyDroneId on the south side from route, distance from route:100.0");
    }

    @Test
    public void shouldReturnFormatMessageForOnRoute() {
        // given
        DroneStatusFormatter droneStatusFormatter = new DroneStatusFormatter();
        String droneId = "anyDroneId";
        CheckResult checkResult = new CheckResult(ON_ROUTE);

        // when
        String message = droneStatusFormatter.formatMessage(droneId, checkResult);

        // then
        assertThat(message).isEqualTo("Drone anyDroneId on the route");
    }

    @Test
    public void shouldReturnFormatMessageForOutOfRadars() {
        // given
        DroneStatusFormatter droneStatusFormatter = new DroneStatusFormatter();
        String droneId = "anyDroneId";
        CheckResult checkResult = new CheckResult(OUT_OF_RADARS);

        // when
        String message = droneStatusFormatter.formatMessage(droneId, checkResult);

        // then
        assertThat(message).isEqualTo("Drone anyDroneId is out of radar");
    }

}