package com.kit.drone;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ExpectedLogStatement extends TestWatcher {

    private ListAppender<ILoggingEvent> listAppender = new ListAppender<>();

    private Logger logger;

    private Level level;
    private Level originalLevel;

    public <T> ExpectedLogStatement(final Class<T> type) {
        logger = (Logger) LoggerFactory.getLogger(type);
        originalLevel = logger.getLevel();
    }

    public <T> ExpectedLogStatement(final Class<T> type, final Level level) {
        this(type);
        this.level = level;
    }

    @Override
    public void starting(Description description) {
        logger.addAppender(listAppender);

        changeLoggerLevel();

        listAppender.start();
    }

    @Override
    public void finished(Description description) {
        listAppender.stop();

        restoreLoggerLevel();

        logger.detachAppender(listAppender);
    }

    private void changeLoggerLevel() {
        if (level != null) {
            logger.setLevel(level);
        }
    }

    private void restoreLoggerLevel() {
        if (level != null) {
            logger.setLevel(originalLevel);
        }
    }

    /**
     * Returns true if any recorded LogEvent contains the next message. No Order or Level check, use it on your risk.
     */
    public boolean contains(String message) {
        return getLogEvents().stream()
            .map(ILoggingEvent::getFormattedMessage)
            .anyMatch(s -> s.contains(message));
    }

    public String getLogMessage(int index) {
        return getLogEvent(index).getFormattedMessage();
    }

    public String getLastLogMessage() {
        return getLastLogEvent().getFormattedMessage();
    }

    public List<ILoggingEvent> getLogEvents() {
        return listAppender.list;
    }

    public ILoggingEvent getLogEvent(int index) {
        return getLogEvents().get(index);
    }

    public ILoggingEvent getLastLogEvent() {
        List<ILoggingEvent> loggingEvents = getLogEvents();
        if (loggingEvents.isEmpty()) {
            throw new IndexOutOfBoundsException("no logging event recorded");
        }
        return loggingEvents.get(loggingEvents.size() - 1);
    }
}
