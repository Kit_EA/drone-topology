package com.kit.drone.monitoring;


import com.kit.drone.ExpectedLogStatement;
import com.kit.drone.geo.CheckResult;
import com.kit.drone.geo.DroneStatusFormatter;
import com.kit.drone.geo.GeometryService;
import com.kit.drone.model.DroneData;
import com.kit.drone.model.Route;
import com.kit.drone.monitoring.MonitoringConfiguration.RouteMap;
import com.kit.drone.repository.RouteRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.scheduling.TaskScheduler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MonitoringServiceUnitTest {

    @Rule
    public ExpectedLogStatement log = new ExpectedLogStatement(MonitoringService.class);

    @Mock
    private RouteRepository routeRepository;

    @Mock
    private GeometryService geometryService;

    @Mock
    private DroneStatusFormatter droneStatusFormatter;

    @Mock
    private RouteMap routeMap;

    @Mock
    private TaskScheduler scheduler;

    @Mock
    private MonitoringConfiguration.FutureMap futureMap;

    @Mock
    private DroneMovementValidator droneMovementValidator;

    @InjectMocks
    private MonitoringService monitoringService;

    @Before
    public void setUp() {
        given(routeMap.put(anyString(), isNull())).willThrow(NullPointerException.class);
    }

    @Test
    public void shouldStartMonitoring() {
        // given
        String droneId = "bc1";
        String routeName = "firstRoute";
        Route route = Route.builder().build();
        given(routeRepository.findByName(routeName)).willReturn(route);

        // when
        monitoringService.startMonitoring(droneId, routeName);

        // then
        verify(routeMap).put(droneId, route);
        assertThat(log.getLastLogMessage()).isEqualTo("Monitoring started for: bc1 on route: firstRoute");
    }

    @Test
    public void shouldFailStartMonitoringIfRouteIsMissed() {
        // given
        String droneId = "bc1";
        String routeName = "firstRoute";
        given(routeRepository.findByName(anyString())).willReturn(null);

        // when
        Throwable throwable = catchThrowable(() -> monitoringService.startMonitoring(droneId, routeName));

        // then
        assertThat(throwable).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void shouldLogOnStopMonitoring() {
        // given
        String droneId = "bc1";
        String routeName = "firstRoute";
        final Route route = Route.builder().name(routeName).build();
        given(routeMap.remove(droneId)).willReturn(route);

        // when
        monitoringService.stopMonitoring(droneId);

        // then
        assertThat(log.getLastLogMessage()).isEqualTo("Monitoring stopped for: bc1 on route: firstRoute");
    }

    @Test
    public void shouldLogOnUpdateMonitoring() {
        // given
        String droneId = "bc1";
        String routeName = "firstRoute";
        CheckResult checkResult = CheckResult.builder().build();
        String checkResultMessage = "Expected message";
        Route route = Route.builder().name(routeName).build();
        DroneData droneData = DroneData.builder().droneId(droneId).build();

        given(routeMap.get(anyString())).willReturn(route);
        given(geometryService.checkDronePositionRelatingToRoute(any(), any())).willReturn(checkResult);
        given(droneStatusFormatter.formatMessage(anyString(), any())).willReturn(checkResultMessage);

        // when
        monitoringService.updateMonitoring(droneData);

        // then
        assertThat(log.getLastLogMessage()).isEqualTo(checkResultMessage);
    }

    @SuppressWarnings("ConfusingArgumentToVarargsMethod")
    @Test
    public void shouldThrowExceptionWhenTryToStopMonitoringForUnknownRoute() {
        // given
        String droneId = "bc1";
        given(routeMap.remove(droneId)).willReturn(null);

        // when
        Throwable throwable = catchThrowable(() -> monitoringService.stopMonitoring(droneId));

        // then
        assertThat(throwable).isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void shouldLogDroneMissedTakeOffAreaOnUpdateMonitoring(){
        // given
        String droneId = "bc1";
        String routeName = "firstRoute";
        String checkResultMessage = "Standard update message";
        DroneData droneData = DroneData.builder().droneId(droneId).build();
        Route route = Route.builder().name(routeName).build();
        CheckResult checkResult = CheckResult.builder().build();

        given(routeMap.get(anyString())).willReturn(route);
        given(geometryService.checkDronePositionRelatingToRoute(any(), any())).willReturn(checkResult);
        given(droneStatusFormatter.formatMessage(anyString(), any())).willReturn(checkResultMessage);

        // when
        monitoringService.updateMonitoring(droneData);

        // then
        assertThat(log.getLogMessage(0)).isEqualTo("Drone bc1 missed takeOff area.");
    }

    @Test
    public void shouldNotLogDroneMissedTakeOffAreaOnUpdateMonitoringIfDroneNotMissedTakeOffArea(){
        // given
        String droneId = "bc1";
        String routeName = "firstRoute";
        String checkResultMessage = "Standard update message";
        DroneData droneData = DroneData.builder().droneId(droneId).build();
        Route route = Route.builder().name(routeName).build();
        CheckResult checkResult = CheckResult.builder().build();

        given(routeMap.get(anyString())).willReturn(route);
        given(geometryService.checkDronePositionRelatingToRoute(any(), any())).willReturn(checkResult);
        given(droneMovementValidator.isDroneTookOff(droneData, route)).willReturn(true);
        given(droneStatusFormatter.formatMessage(anyString(), any())).willReturn(checkResultMessage);

        // when
        monitoringService.updateMonitoring(droneData);

        // then
        assertThat(log.getLogMessage(0)).isEqualTo("Standard update message");
    }

    @Test
    public void shouldLogDroneIsNotLandedOnStopMonitoring(){
        // given
        String droneId = "bc1";
        String routeName = "firstRoute";
        Route route = Route.builder().name(routeName).build();

        given(routeMap.remove(anyString())).willReturn(route);

        // when
        monitoringService.stopMonitoring(droneId);

        // then
        assertThat(log.getLogMessage(0)).isEqualTo("Drone bc1 is not landed.");
    }

    @Test
    public void shouldNotLogDroneIsNotLandedOnStopMonitoringIfLanded(){
        // given
        String droneId = "bc1";
        String routeName = "firstRoute";
        Route route = Route.builder().name(routeName).build();

        given(routeMap.remove(anyString())).willReturn(route);
        given(droneMovementValidator.isDroneLanded(droneId, route)).willReturn(true);

        // when
        monitoringService.stopMonitoring(droneId);

        // then
        assertThat(log.getLogMessage(0)).isEqualTo("Monitoring stopped for: bc1 on route: firstRoute");
    }

    @Test
    public void shouldThrowExceptionWhenTryToUpdateMonitoringForUnknownRoute(){
        // given
        String droneId = "bc1";
        DroneData droneData = DroneData.builder().droneId(droneId).build();
        given(routeMap.get(droneData.getDroneId())).willReturn(null);

        // when
        Throwable throwable = catchThrowable(() -> monitoringService.updateMonitoring(droneData));

        // then
        assertThat(throwable).isInstanceOf(IllegalStateException.class);
    }
}
