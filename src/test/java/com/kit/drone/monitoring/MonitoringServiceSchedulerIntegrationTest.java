package com.kit.drone.monitoring;

import com.kit.drone.geo.CheckResult;
import com.kit.drone.geo.DroneStatusFormatter;
import com.kit.drone.geo.GeometryService;
import com.kit.drone.model.DroneData;
import com.kit.drone.model.Route;
import com.kit.drone.monitoring.MonitoringConfiguration.FutureMap;
import com.kit.drone.repository.RouteRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

import static com.kit.drone.monitoring.MonitoringConfiguration.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MonitoringServiceSchedulerIntegrationTest {

    private static final String ANY_DRONE_ID = "anyDrone";
    private static final String ANY_ROUTE_NAME = "anyRoute";
    private static final Route ANY_ROUTE = Route.builder().name(ANY_ROUTE_NAME).build();

    @MockBean
    private RouteRepository routeRepository;

    @SpyBean
    private FutureMap futureMap;

    @SpyBean
    private MonitoringService monitoringService;

    @MockBean
    private GeometryService geometryService;

    @MockBean
    private DroneStatusFormatter droneStatusFormatter;

    @MockBean
    private DroneMovementValidator droneMovementValidator;

    @MockBean
    private RouteMap routeMap;


    @Before
    public void setUp() {
        given(routeRepository.findByName(anyString())).willReturn(ANY_ROUTE);
        given(routeMap.get(anyString())).willReturn(ANY_ROUTE);
        given(routeMap.remove(anyString())).willReturn(ANY_ROUTE);
    }


    @Test
    public void shouldStartMonitoringTwice() throws Exception {
        // given
        monitoringService.startMonitoring(ANY_DRONE_ID, ANY_ROUTE_NAME);

        // when
        waitForSchedule(6);
        monitoringService.startMonitoring(ANY_DRONE_ID, ANY_ROUTE_NAME);

        // then
        // TODO: think about it
    }

    @Test
    public void shouldScheduleStopMonitoringOnMonitoringStart() {
        // when
        monitoringService.startMonitoring(ANY_DRONE_ID, ANY_ROUTE_NAME);

        // then
        verify(futureMap).put(anyString(), any());
    }

    @Test
    public void shouldStopMonitoringBySchedule() throws Exception {
        // given
        monitoringService.startMonitoring(ANY_DRONE_ID, ANY_ROUTE_NAME);

        // when
        waitForSchedule(6);

        // then
        verify(monitoringService).stopMonitoring(anyString());
        verify(futureMap).remove(anyString());

    }

    @Test
    public void shouldStopMonitoringWhenMonitoringStopStillScheduled() throws Exception {
        // given
        monitoringService.startMonitoring(ANY_DRONE_ID, ANY_ROUTE_NAME);

        // when
        monitoringService.stopMonitoring(ANY_DRONE_ID);

        // then
        verify(futureMap).remove(anyString());
        //TODO: think if we need to check feature
    }

    @Test
    public void shouldRescheduleMonitoringStopOnMonitoringUpdate() throws Exception {
        // given
        String checkResultMessage = "Expected message";
        CheckResult checkResult = CheckResult.builder().build();
        DroneData droneData = DroneData.builder().droneId(ANY_DRONE_ID).build();
        given(geometryService.checkDronePositionRelatingToRoute(any(), any())).willReturn(checkResult);
        given(droneStatusFormatter.formatMessage(anyString(), any())).willReturn(checkResultMessage);

        monitoringService.startMonitoring(ANY_DRONE_ID, ANY_ROUTE_NAME);

        // when
        waitForSchedule(3);
        monitoringService.updateMonitoring(droneData);
        waitForSchedule(6);

        // then
        verify(futureMap, times(2)).put(anyString(), any());
        verify(futureMap, times(2)).remove(anyString());
    }

    private void waitForSchedule(int timeout) throws InterruptedException {
        TimeUnit.SECONDS.sleep(timeout);
    }
}