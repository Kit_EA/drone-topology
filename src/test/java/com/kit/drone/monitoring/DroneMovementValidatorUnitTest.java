package com.kit.drone.monitoring;

import com.kit.drone.geo.GeometryService;
import com.kit.drone.model.DroneData;
import com.kit.drone.model.Route;
import com.kit.drone.monitoring.MonitoringConfiguration.DroneLastPositionMap;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class DroneMovementValidatorUnitTest {

    private static final String ANY_DRONE_ID = "anyDrone";
    private static final Point ANY_COORDINATE = new GeometryFactory().createPoint(new Coordinate(0, 0));
    private static final DroneData ANY_DRONE_DATA = DroneData.builder().droneId(ANY_DRONE_ID).droneLocation(ANY_COORDINATE).build();
    private static final Route ANY_ROUTE = Route.builder().build();

    @Mock
    private GeometryService geometryService;

    @Mock
    private DroneLastPositionMap droneLastPositionMap;

    @InjectMocks
    private DroneMovementValidator validator;

    @Test
    public void shouldReturnTrueIfDroneLocationBelongsToTakeOff() {
        // given
        given(geometryService.isDroneLocationBelongsToTakeOff(any(), any())).willReturn(true);

        // when
        boolean result = validator.isDroneTookOff(ANY_DRONE_DATA, ANY_ROUTE);

        // then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseIfDroneLocationNotBelongsToTakeOff() {
        // given
        given(geometryService.isDroneLocationBelongsToTakeOff(any(), any())).willReturn(false);

        // when
        boolean result = validator.isDroneTookOff(ANY_DRONE_DATA, ANY_ROUTE);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnTrueIfAlreadyTookOff() {
        // given
        given(droneLastPositionMap.remove(anyString())).willReturn(ANY_COORDINATE);

        // when
        boolean result = validator.isDroneTookOff(ANY_DRONE_DATA, ANY_ROUTE);

        // then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnTrueIfDroneLocationBelongsToLanding() {
        // given
        given(geometryService.isDroneLocationBelongsToLanding(any(), any())).willReturn(true);

        // when
        boolean result = validator.isDroneLanded(ANY_DRONE_ID, ANY_ROUTE);

        // then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseIfDroneLocationNotBelongsToLanding() {
        // given
        given(geometryService.isDroneLocationBelongsToLanding(any(), any())).willReturn(false);

        // when
        boolean result = validator.isDroneLanded(ANY_DRONE_ID, ANY_ROUTE);

        // then
        assertThat(result).isFalse();
    }

}