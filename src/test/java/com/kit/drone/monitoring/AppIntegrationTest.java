package com.kit.drone.monitoring;

import com.kit.drone.ExpectedLogStatement;
import com.kit.drone.resource.DroneDataResource;
import com.kit.drone.resource.RouteResource;
import com.vividsolutions.jts.geom.GeometryFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AppIntegrationTest {

    private GeometryFactory geometryFactory = new GeometryFactory();

    @Rule
    public ExpectedLogStatement log = new ExpectedLogStatement(MonitoringService.class);

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldCheckIfTwoDronesAreWorkingFine() {
        // given
        String firstDroneId = "bc1";
        String secondDroneId = "ad1";

        DroneDataResource bc1Data = DroneDataResource.builder().droneId(firstDroneId)
                .location(DroneDataResource.PointResource.builder().x(2).y(3).z(2).build()).build();
        DroneDataResource bc1Data2 = DroneDataResource.builder().droneId(firstDroneId)
                .location(DroneDataResource.PointResource.builder().x(4).y(2).z(3).build()).build();
        DroneDataResource bc1Data3 = DroneDataResource.builder().droneId(firstDroneId)
                .location(DroneDataResource.PointResource.builder().x(2.5).y(8).z(0).build()).build();
        DroneDataResource ad1Data = DroneDataResource.builder().droneId(secondDroneId)
                .location(DroneDataResource.PointResource.builder().x(4).y(5).z(4).build()).build();

        // when
        this.restTemplate.postForEntity("/route", getRealRoute(), String.class);
        this.restTemplate.postForEntity("/route", getRealRoute("secondRoute", secondDroneId,
                2, 2, 4), String.class);

        this.restTemplate.postForEntity("/monitoring/drone/bc1?routeId=firstRoute", null, String.class);
        this.restTemplate.postForEntity("/monitoring/drone/ad1?routeId=secondRoute", null, String.class);

        this.restTemplate.postForEntity("/monitoring/drone", bc1Data, String.class);
        this.restTemplate.postForEntity("/monitoring/drone", bc1Data2, String.class);
        this.restTemplate.postForEntity("/monitoring/drone", bc1Data3, String.class);
        this.restTemplate.postForEntity("/monitoring/drone", ad1Data, String.class);

        this.restTemplate.delete("/monitoring/drone/bc1");
        this.restTemplate.delete("/monitoring/drone/ad1");

        // then
        assertThat(log.getLastLogMessage()).isEqualTo("Monitoring stopped for: ad1 on route: secondRoute");
    }

    private RouteResource getRealRoute() {

        return aRoute()
                .name("firstRoute")
                .line(asList(
                        aPoint().x(0).y(0).z(0).build(),
                        aPoint().x(1.2).y(2).z(3).build(),
                        aPoint().x(2.5).y(3).z(3).build(),
                        aPoint().x(5.5).y(5).z(3).build(),
                        aPoint().x(2.3).y(8).z(0).build()))
                .polygons(asList(
                        aPolygon().points(asList(
                                aPoint().x(0).y(0).z(0).build(),
                                aPoint().x(2).y(0).z(0).build(), // subject to change
                                aPoint().x(2).y(2).z(0).build(), // subject to change
                                aPoint().x(0).y(2).z(0).build(), // subject to change
                                aPoint().x(3).y(0).z(2).build(),
                                aPoint().x(3).y(0).z(4).build(),
                                aPoint().x(1).y(0).z(4).build(),
                                aPoint().x(1).y(0).z(2).build(),
                                aPoint().x(0).y(0).z(0).build()
                        )).build(),
                        aPolygon().points(asList(
                                aPoint().x(1).y(0).z(2).build(),
                                aPoint().x(1).y(0).z(4).build(),
                                aPoint().x(3).y(0).z(4).build(),
                                aPoint().x(3).y(0).z(2).build(),
                                aPoint().x(5).y(3).z(2).build(),
                                aPoint().x(5).y(3).z(4).build(),
                                aPoint().x(2).y(3).z(4).build(),
                                aPoint().x(2).y(3).z(2).build(),
                                aPoint().x(1).y(0).z(2).build()))
                                .build(),
                        aPolygon().points(asList(
                                aPoint().x(2).y(3).z(2).build(),
                                aPoint().x(2).y(3).z(4).build(),
                                aPoint().x(5).y(3).z(4).build(),
                                aPoint().x(5).y(3).z(2).build(),
                                aPoint().x(7).y(5).z(2).build(),
                                aPoint().x(7).y(5).z(4).build(),
                                aPoint().x(5).y(5).z(4).build(),
                                aPoint().x(5).y(5).z(2).build(),
                                aPoint().x(2).y(3).z(2).build()))
                                .build(),
                        aPolygon().points(asList(
                                aPoint().x(5).y(5).z(2).build(),
                                aPoint().x(5).y(5).z(4).build(),
                                aPoint().x(7).y(5).z(4).build(),
                                aPoint().x(7).y(5).z(2).build(),
                                aPoint().x(3.5).y(7).z(0).build(), // subject to change
                                aPoint().x(3.5).y(9).z(0).build(), // subject to change
                                aPoint().x(2).y(9).z(0).build(), // subject to change
                                aPoint().x(2).y(7).z(0).build(), // subject to change
                                aPoint().x(5).y(5).z(2).build()))
                                .build()
                ))
                .build();
    }

    private RouteResource getRealRoute(String routeName) {

        return aRoute()
                .name("firstRoute")
                .line(asList(
                        aPoint().x(0).y(0).z(0).build(),
                        aPoint().x(1.2).y(2).z(3).build(),
                        aPoint().x(2.5).y(3).z(3).build(),
                        aPoint().x(5.5).y(5).z(3).build(),
                        aPoint().x(2.3).y(8).z(0).build()))
                .polygons(asList(
                        aPolygon().points(asList(
                                aPoint().x(0).y(0).z(0).build(),
                                aPoint().x(2).y(0).z(0).build(), // subject to change
                                aPoint().x(2).y(2).z(0).build(), // subject to change
                                aPoint().x(0).y(2).z(0).build(), // subject to change
                                aPoint().x(3).y(0).z(2).build(),
                                aPoint().x(3).y(0).z(4).build(),
                                aPoint().x(1).y(0).z(4).build(),
                                aPoint().x(1).y(0).z(2).build(),
                                aPoint().x(0).y(0).z(0).build()
                        )).build(),
                        aPolygon().points(asList(
                                aPoint().x(1).y(0).z(2).build(),
                                aPoint().x(1).y(0).z(4).build(),
                                aPoint().x(3).y(0).z(4).build(),
                                aPoint().x(3).y(0).z(2).build(),
                                aPoint().x(5).y(3).z(2).build(),
                                aPoint().x(5).y(3).z(4).build(),
                                aPoint().x(2).y(3).z(4).build(),
                                aPoint().x(2).y(3).z(2).build(),
                                aPoint().x(1).y(0).z(2).build()))
                                .build(),
                        aPolygon().points(asList(
                                aPoint().x(2).y(3).z(2).build(),
                                aPoint().x(2).y(3).z(4).build(),
                                aPoint().x(5).y(3).z(4).build(),
                                aPoint().x(5).y(3).z(2).build(),
                                aPoint().x(7).y(5).z(2).build(),
                                aPoint().x(7).y(5).z(4).build(),
                                aPoint().x(5).y(5).z(4).build(),
                                aPoint().x(5).y(5).z(2).build(),
                                aPoint().x(2).y(3).z(2).build()))
                                .build(),
                        aPolygon().points(asList(
                                aPoint().x(5).y(5).z(2).build(),
                                aPoint().x(5).y(5).z(4).build(),
                                aPoint().x(7).y(5).z(4).build(),
                                aPoint().x(7).y(5).z(2).build(),
                                aPoint().x(3.5).y(7).z(0).build(), // subject to change
                                aPoint().x(3.5).y(9).z(0).build(), // subject to change
                                aPoint().x(2).y(9).z(0).build(), // subject to change
                                aPoint().x(2).y(7).z(0).build(), // subject to change
                                aPoint().x(5).y(5).z(2).build()))
                                .build()
                ))
                .build();
    }

    private RouteResource getRealRoute(String routeName, String droneId, double addToX, double addToY, double addToZ) {

        return aRoute()
                .name(routeName)
                .line(asList(
                        aPoint().x(0 + addToX).y(0 + addToY).z(0).build(),
                        aPoint().x(1.2 + addToX).y(2 + addToY).z(3 + addToZ).build(),
                        aPoint().x(2.5 + addToX).y(3 + addToY).z(3 + addToZ).build(),
                        aPoint().x(5.5 + addToX).y(5 + addToY).z(3 + addToZ).build(),
                        aPoint().x(2.3 + addToX).y(8 + addToY).z(0).build()))
                .polygons(asList(
                        aPolygon().points(asList(
                                aPoint().x(0 + addToX).y(0 + addToY).z(0).build(),
                                aPoint().x(2 + addToX).y(0 + addToY).z(0).build(), // subject to change
                                aPoint().x(2 + addToX).y(2 + addToY).z(0).build(), // subject to change
                                aPoint().x(0 + addToX).y(2 + addToY).z(0).build(), // subject to change
                                aPoint().x(3 + addToX).y(0 + addToY).z(2 + addToZ).build(),
                                aPoint().x(3 + addToX).y(0 + addToY).z(4 + addToZ).build(),
                                aPoint().x(1 + addToX).y(0 + addToY).z(4 + addToZ).build(),
                                aPoint().x(1 + addToX).y(0 + addToY).z(2 + addToZ).build(),
                                aPoint().x(0 + addToX).y(0 + addToY).z(0).build()
                        )).build(),
                        aPolygon().points(asList(
                                aPoint().x(1 + addToX).y(0 + addToY).z(2 + addToZ).build(),
                                aPoint().x(1 + addToX).y(0 + addToY).z(4 + addToZ).build(),
                                aPoint().x(3 + addToX).y(0 + addToY).z(4 + addToZ).build(),
                                aPoint().x(3 + addToX).y(0 + addToY).z(2 + addToZ).build(),
                                aPoint().x(5 + addToX).y(3 + addToY).z(2 + addToZ).build(),
                                aPoint().x(5 + addToX).y(3 + addToY).z(4 + addToZ).build(),
                                aPoint().x(2 + addToX).y(3 + addToY).z(4 + addToZ).build(),
                                aPoint().x(2 + addToX).y(3 + addToY).z(2 + addToZ).build(),
                                aPoint().x(1 + addToX).y(0 + addToY).z(2 + addToZ).build()))
                                .build(),
                        aPolygon().points(asList(
                                aPoint().x(2 + addToX).y(3 + addToY).z(2 + addToZ).build(),
                                aPoint().x(2 + addToX).y(3 + addToY).z(4 + addToZ).build(),
                                aPoint().x(5 + addToX).y(3 + addToY).z(4 + addToZ).build(),
                                aPoint().x(5 + addToX).y(3 + addToY).z(2 + addToZ).build(),
                                aPoint().x(7 + addToX).y(5 + addToY).z(2 + addToZ).build(),
                                aPoint().x(7 + addToX).y(5 + addToY).z(4 + addToZ).build(),
                                aPoint().x(5 + addToX).y(5 + addToY).z(4 + addToZ).build(),
                                aPoint().x(5 + addToX).y(5 + addToY).z(2 + addToZ).build(),
                                aPoint().x(2 + addToX).y(3 + addToY).z(2 + addToZ).build()))
                                .build(),
                        aPolygon().points(asList(
                                aPoint().x(5 + addToX).y(5 + addToY).z(2 + addToZ).build(),
                                aPoint().x(5 + addToX).y(5 + addToY).z(4 + addToZ).build(),
                                aPoint().x(7 + addToX).y(5 + addToY).z(4 + addToZ).build(),
                                aPoint().x(7 + addToX).y(5 + addToY).z(2 + addToZ).build(),
                                aPoint().x(3.5 + addToX).y(7 + addToY).z(0).build(), // subject to change
                                aPoint().x(3.5 + addToX).y(9 + addToY).z(0).build(), // subject to change
                                aPoint().x(2 + addToX).y(9 + addToY).z(0).build(), // subject to change
                                aPoint().x(2 + addToX).y(7 + addToY).z(0).build(), // subject to change
                                aPoint().x(5 + addToX).y(5 + addToY).z(2 + addToZ).build()))
                                .build()
                ))
                .build();
    }

    private RouteResource.RouteResourceBuilder aRoute() {
        return RouteResource.builder();
    }

    private RouteResource.PolygonResource.PolygonResourceBuilder aPolygon() {
        return RouteResource.PolygonResource.builder();
    }

    private RouteResource.PointResource.PointResourceBuilder aPoint() {
        return RouteResource.PointResource.builder();
    }
}
