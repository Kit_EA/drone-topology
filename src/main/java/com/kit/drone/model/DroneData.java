package com.kit.drone.model;

import com.vividsolutions.jts.geom.Point;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DroneData {

    private String droneId;
    private Point droneLocation;

}
