package com.kit.drone.monitoring;

import com.kit.drone.geo.CheckResult;
import com.kit.drone.geo.DroneStatusFormatter;
import com.kit.drone.geo.GeometryService;
import com.kit.drone.model.DroneData;
import com.kit.drone.model.Route;
import com.kit.drone.monitoring.MonitoringConfiguration.FutureMap;
import com.kit.drone.monitoring.MonitoringConfiguration.RouteMap;
import com.kit.drone.repository.RouteRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.concurrent.ScheduledFuture;

@Service
@Slf4j
@RequiredArgsConstructor
public class MonitoringService {

    private final GeometryService geometryService;
    private final RouteRepository routeRepository;
    private final DroneStatusFormatter droneStatusFormatter;
    private final RouteMap routeMap;
    private final TaskScheduler scheduler;
    private final FutureMap futureMap;
    private final DroneMovementValidator droneMovementValidator;

    public void startMonitoring(String droneId, String routeName) {
        routeMap.put(droneId, routeRepository.findByName(routeName));
        scheduleMonitoringStop(droneId);
        log.info(String.format("Monitoring started for: %s on route: %s", droneId, routeName));
    }

    public void stopMonitoring(String droneId) {
        unscheduleMonitoringStop(droneId);
        Route route = routeMap.remove(droneId);
        if (route == null) {
            throw new IllegalStateException(String.format("Route for drone %s is not exist.", droneId));
        }

        if (!droneMovementValidator.isDroneLanded(droneId, route)) {
            log.warn(String.format("Drone %s is not landed.", droneId));
        }
        log.info(String.format("Monitoring stopped for: %s on route: %s", droneId, route.getName()));
    }

    public void updateMonitoring(DroneData droneData) {
        rescheduleMonitoringStop(droneData);
        Route route = routeMap.get(droneData.getDroneId());
        if (route == null) {
            throw new IllegalStateException(String.format("Route for drone %s is not exist.", droneData.getDroneId()));
        }

        if (!droneMovementValidator.isDroneTookOff(droneData, route)) {
            log.warn(String.format("Drone %s missed takeOff area.", droneData.getDroneId()));
        }
        CheckResult result = geometryService.checkDronePositionRelatingToRoute(droneData.getDroneLocation(), route);
        log.info(droneStatusFormatter.formatMessage(droneData.getDroneId(), result));
    }

    private void scheduleMonitoringStop(String droneId) {
        futureMap.put(droneId, scheduler.schedule(() -> stopMonitoring(droneId), Instant.now().plusSeconds(5)));
    }

    private void unscheduleMonitoringStop(String droneId) {
        ScheduledFuture future = futureMap.remove(droneId);
        if (future != null) {
            future.cancel(false);
        }
    }

    private void rescheduleMonitoringStop(DroneData droneData) {
        unscheduleMonitoringStop(droneData.getDroneId());
        scheduleMonitoringStop(droneData.getDroneId());
    }

}
