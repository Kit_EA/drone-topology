package com.kit.drone.monitoring;

import com.kit.drone.geo.GeometryService;
import com.kit.drone.model.DroneData;
import com.kit.drone.model.Route;
import com.kit.drone.monitoring.MonitoringConfiguration.DroneLastPositionMap;
import com.vividsolutions.jts.geom.Point;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@RequiredArgsConstructor
public class DroneMovementValidator {

    private final GeometryService geometryService;
    private final DroneLastPositionMap droneLastPositionMap;

    public boolean isDroneTookOff(final DroneData droneData, final Route route) {
        boolean tookOff = true;
        Point lastPosition = droneLastPositionMap.remove(droneData.getDroneId());
        if (lastPosition == null) {
            tookOff = geometryService.isDroneLocationBelongsToTakeOff(droneData.getDroneLocation(), route);
        }

        droneLastPositionMap.put(droneData.getDroneId(), droneData.getDroneLocation());
        return tookOff;
    }

    public boolean isDroneLanded(final String droneId, Route route) {
        Point lastPosition = droneLastPositionMap.remove(droneId);
        return geometryService.isDroneLocationBelongsToLanding(lastPosition, route);
    }

}
