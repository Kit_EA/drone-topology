package com.kit.drone.monitoring;

import com.kit.drone.model.Route;
import com.vividsolutions.jts.geom.Point;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

@Configuration
public class MonitoringConfiguration {

    @Bean
    public RouteMap routeMap() {
        return new RouteMap();
    }

    @Bean
    public FutureMap futureMap() {
        return new FutureMap();
    }

    @Bean
    public DroneLastPositionMap droneLastPositionMap() {
        return new DroneLastPositionMap();
    }

    public static class RouteMap extends ConcurrentHashMap<String, Route> {
    }

    public static class DroneLastPositionMap extends ConcurrentHashMap<String, Point> {
    }

    public static class FutureMap extends ConcurrentHashMap<String, ScheduledFuture> {
    }

}
