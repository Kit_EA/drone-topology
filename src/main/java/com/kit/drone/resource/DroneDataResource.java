package com.kit.drone.resource;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DroneDataResource {

    private String droneId;
    private PointResource location;

    @Data
    @Builder
    @JsonTypeName("Point")
    public static class PointResource {

        private double x;
        private double y;
        private double z;

    }
}
