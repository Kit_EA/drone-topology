package com.kit.drone.resource;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class RouteResource {

    private String name;
    private List<PointResource> line;
    private List<PolygonResource> polygons;

    @Data
    @Builder
    @JsonTypeName("Point")
    public static class PointResource {

        private double x;
        private double y;
        private double z;

    }

    @Data
    @Builder
    @JsonTypeName("Polygon")
    public static class PolygonResource {

        private List<PointResource> points;

    }

}
