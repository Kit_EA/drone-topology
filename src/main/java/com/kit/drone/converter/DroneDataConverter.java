package com.kit.drone.converter;

import com.kit.drone.model.DroneData;
import com.kit.drone.resource.DroneDataResource;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import org.springframework.stereotype.Component;

@Component
public class DroneDataConverter {

    private final GeometryFactory geometryFactory = new GeometryFactory();

    public DroneData convert(DroneDataResource droneDataResource) {
        return DroneData.builder().droneId(droneDataResource.getDroneId())
                .droneLocation(geometryFactory.createPoint(new Coordinate(droneDataResource.getLocation().getX(),
                        droneDataResource.getLocation().getY(), droneDataResource.getLocation().getZ()))).build();
    }
}
