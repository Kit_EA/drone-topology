package com.kit.drone.converter;

import com.kit.drone.model.Route;
import com.kit.drone.resource.RouteResource;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.kit.drone.resource.RouteResource.PointResource;
import static com.kit.drone.resource.RouteResource.PolygonResource;

@Component
public class RouteConverter {

    private final GeometryFactory geometryFactory = new GeometryFactory();

    public Route convert(RouteResource routeResource) {
        return Route.builder()
                .name(routeResource.getName())
                .path(convertToPath(routeResource.getLine()))
                .corridor(convertToCorridor(routeResource.getPolygons()))
                .build();
    }

    private MultiPolygon convertToCorridor(List<PolygonResource> polygons) {
        return geometryFactory.createMultiPolygon(polygons.stream().map(this::convertToPolygon).toArray(Polygon[]::new));
    }

    private Polygon convertToPolygon(PolygonResource polygon) {
        return geometryFactory.createPolygon(convertPointsToSequence(polygon.getPoints()));
    }

    private LineString convertToPath(List<PointResource> line) {
        return geometryFactory.createLineString(convertPointsToSequence(line));
    }

    private CoordinateSequence convertPointsToSequence(List<PointResource> points) {
        final Coordinate[] coordinates = points.stream()
                .map(point -> new Coordinate(point.getX(), point.getY(), point.getZ()))
                .toArray(Coordinate[]::new);
        return geometryFactory.getCoordinateSequenceFactory().create(coordinates);
    }

}
