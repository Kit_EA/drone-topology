package com.kit.drone.geo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class CheckResult {

    private Double deviation;
    private final DroneStatus droneStatus;

}
