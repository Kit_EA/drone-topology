package com.kit.drone.geo;

import org.springframework.stereotype.Component;

@Component
public class DroneStatusFormatter {

    public String formatMessage(String droneId, CheckResult checkResult) {
        return String.format(checkResult.getDroneStatus().getFormat(), droneId, checkResult.getDeviation());
    }
}
