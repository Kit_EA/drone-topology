package com.kit.drone.geo;

public enum DroneStatus {

    OUT_OF_ROUTE_WEST("Drone %s on the west side from route, distance from route:%s"),
    OUT_OF_ROUTE_EAST("Drone %s on the east side from route, distance from route:%s"),
    OUT_OF_ROUTE_NORTH("Drone %s on the north side from route, distance from route:%s"),
    OUT_OF_ROUTE_SOUTH("Drone %s on the south side from route, distance from route:%s"),

    ON_ROUTE("Drone %s on the route"),

    OUT_OF_RADARS("Drone %s is out of radar");


    private String format;

    DroneStatus(final String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }
}
