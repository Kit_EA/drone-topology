package com.kit.drone.geo;

import com.kit.drone.model.Route;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import static com.kit.drone.geo.DroneStatus.ON_ROUTE;
import static com.kit.drone.geo.DroneStatus.OUT_OF_RADARS;
import static com.kit.drone.geo.DroneStatus.OUT_OF_ROUTE_EAST;
import static com.kit.drone.geo.DroneStatus.OUT_OF_ROUTE_NORTH;
import static com.kit.drone.geo.DroneStatus.OUT_OF_ROUTE_SOUTH;
import static com.kit.drone.geo.DroneStatus.OUT_OF_ROUTE_WEST;


@Service
public class GeometryService {

    private static final int AMOUNT_OF_POINTS_OF_ONE_EDGE = 4;
    private static final int AMOUNT_OF_POINTS_OF_SEGMENT_OF_CORRIDOR = 8;
    private static final int AMOUNT_OF_POINTS_OF_SEGMENT_OF_CORRIDOR_WITH_CLOSING = AMOUNT_OF_POINTS_OF_SEGMENT_OF_CORRIDOR + 1;

    private static final double NEAR_LOCATION_MAX_DISTANCE = 2.0;
    private static final double ACCURACY = 0.75;

    private GeometryFactory geometryFactory = new GeometryFactory();

    public CheckResult checkDronePositionRelatingToRoute(Point droneLocation, Route route) {
        Point droneLocation2d = convert3dPointTo2dPoint(droneLocation);
        for (int routeIndex = 0; routeIndex < route.getCorridor().getNumGeometries(); routeIndex++) {
            Polygon segmentCut = getSegmentCutFromCorridor(route, droneLocation2d, routeIndex);

            if (isDroneOnCorridor(droneLocation2d, segmentCut)) {
                return determinePathRelationToCorridor(route, droneLocation2d, segmentCut);
            }

            if (isDroneNearRoute(droneLocation2d, segmentCut)) {
                return determineDronePosition(segmentCut, droneLocation2d);
            }
        }

        return new CheckResult(OUT_OF_RADARS);
    }

    private CheckResult determineDronePosition(Polygon cut, Point droneLocation2d) {
        Point cutCenterPoint = cut.getCentroid();

        double westEastPositionRelativeToCorridor = evaluateWestEastDronePositionRelativelyToCorridor(droneLocation2d, cutCenterPoint);
        double northSouthPositionRelativeToCorridor = evaluateNorthSouthDronePositionRelativelyToCorridor(droneLocation2d, cutCenterPoint);

        if (Math.signum(westEastPositionRelativeToCorridor) == -1) {
            return new CheckResult(getDistanceBetween(droneLocation2d, cut), OUT_OF_ROUTE_WEST);
        } else if (Math.signum(westEastPositionRelativeToCorridor) == 1) {
            return new CheckResult(getDistanceBetween(droneLocation2d, cut), OUT_OF_ROUTE_EAST);
        } else if (Math.signum(northSouthPositionRelativeToCorridor) == -1) {
            return new CheckResult(getDistanceBetween(droneLocation2d, cut), OUT_OF_ROUTE_SOUTH);
        } else {
            return new CheckResult(getDistanceBetween(droneLocation2d, cut), OUT_OF_ROUTE_NORTH);
        }
    }

    private CheckResult determinePathRelationToCorridor(Route route, Point droneLocation2d, Polygon segmentCut) {
        if (isRoutePathCloseToEdgeBoundary(segmentCut, route.getPath())) {
            return determineDronePosition(segmentCut, droneLocation2d);
        } else {
            return new CheckResult(ON_ROUTE);
        }
    }

    private Polygon getSegmentCutFromCorridor(Route route, Point droneLocation2d, int routeIndex) {
        MultiPolygon corridor = route.getCorridor();
        Polygon segment = (Polygon) corridor.getGeometryN(routeIndex);
        return getClosestToDroneCutOfCorridor(segment, droneLocation2d);
    }

    private double evaluateNorthSouthDronePositionRelativelyToCorridor(Point droneLocation2d, Point cutCenterPoint) {
        return Math.abs(droneLocation2d.getCoordinate().getOrdinate(1))
                - Math.abs(cutCenterPoint.getCoordinate().getOrdinate(1));
    }

    private double evaluateWestEastDronePositionRelativelyToCorridor(Point droneLocation2d, Point cutCenterPoint) {
        return Math.abs(droneLocation2d.getCoordinate().getOrdinate(0))
                - Math.abs(cutCenterPoint.getCoordinate().getOrdinate(0));
    }

    private double getDistanceBetween(final Point droneLocation2d, final Polygon cut) {
        return cut.getBoundary().distance(droneLocation2d);
    }

    private boolean isDroneOnCorridor(final Point droneLocation2d, final Polygon segmentCut) {
        return segmentCut.intersects(droneLocation2d);
    }

    private boolean isRoutePathCloseToEdgeBoundary(Polygon edge, final LineString path) {
        Point edgeCentroid = edge.getCentroid();
        Point snapshotOfPath = take2dSnapshotOf3dPath(path);
        return snapshotOfPath.distance(edgeCentroid) <= calculateDistanceFromEdeBoundaryToCenter(edge, edgeCentroid);
    }

    private double calculateDistanceFromEdeBoundaryToCenter(final Polygon closestEdge, final Point edgeCentroid) {
        return getDistanceBetween(edgeCentroid, closestEdge) * ACCURACY;
    }

    private boolean isDroneNearRoute(final Point droneLocation2d, final Polygon closestEdge) {
        return closestEdge.isWithinDistance(droneLocation2d, NEAR_LOCATION_MAX_DISTANCE);
    }

    private Polygon getClosestToDroneCutOfCorridor(Polygon segment, Point droneLocation) {
        CoordinateSequence segmentCoordinates = getSegmentCoordinates(segment);

        Polygon firstEdge = getEdge(segmentCoordinates, 0, AMOUNT_OF_POINTS_OF_ONE_EDGE);
        Polygon secondEdge = getEdge(segmentCoordinates, AMOUNT_OF_POINTS_OF_ONE_EDGE, AMOUNT_OF_POINTS_OF_SEGMENT_OF_CORRIDOR);

        return getClosestToDroneCutOfCorridor(firstEdge, secondEdge, droneLocation);
    }

    private Polygon getClosestToDroneCutOfCorridor(final Polygon firstEdge, final Polygon secondEdge, final Point droneLocation) {
        double distanceOfDroneToFirstEdge = calculateDistanceOfDroneToEdgeByXOrdinate(droneLocation, firstEdge);
        double distanceOfDroneToSecondEdge = calculateDistanceOfDroneToEdgeByXOrdinate(droneLocation, secondEdge);

        return Math.abs(distanceOfDroneToFirstEdge) < Math.abs(distanceOfDroneToSecondEdge) ? firstEdge : secondEdge;
    }

    private double calculateDistanceOfDroneToEdgeByXOrdinate(final Point droneLocation, final Polygon firstEdge) {
        return Math.abs(droneLocation.getCoordinate().getOrdinate(0))
                - Math.abs(firstEdge.getCentroid().getCoordinate().getOrdinate(0));
    }

    private Polygon getEdge(CoordinateSequence segmentCoordinates, int startIndex, int endIndex) {
        ArrayList<Coordinate> coordinatesOfEdge = new ArrayList<>();
        for (int index = startIndex; index < endIndex; index++) {
            coordinatesOfEdge.add(create2DCoordinate(segmentCoordinates, index));
        }
        coordinatesOfEdge.add(create2DCoordinate(segmentCoordinates, startIndex));

        return geometryFactory.createPolygon(coordinatesOfEdge.toArray(new Coordinate[0]));
    }

    private Coordinate create2DCoordinate(final CoordinateSequence segmentCoordinates, final int index) {
        Coordinate coordinate = new Coordinate(segmentCoordinates.getCoordinate(index));
        return flipYZOrdinates(coordinate);
    }

    private Coordinate flipYZOrdinates(final Coordinate coordinate) {
        coordinate.setOrdinate(1, coordinate.getOrdinate(2));
        return coordinate;
    }

    private CoordinateSequence getSegmentCoordinates(final Polygon segment) {
        if (segment.getCoordinates().length != AMOUNT_OF_POINTS_OF_SEGMENT_OF_CORRIDOR_WITH_CLOSING) {
            throw new IllegalStateException("Amount of points of segment of corridor is exceeded the limit.");
        }
        return geometryFactory.getCoordinateSequenceFactory().create(segment.getCoordinates());
    }

    private Point convert3dPointTo2dPoint(Point point) {
        point.getCoordinate().setOrdinate(1, point.getCoordinate().getOrdinate(2));
        return geometryFactory.createPoint(point.getCoordinate());
    }

    private Point take2dSnapshotOf3dPath(LineString lineString) {
        Point snapshotOfLine2d = lineString.getStartPoint();
        snapshotOfLine2d.getCoordinate()
                .setOrdinate(1, snapshotOfLine2d.getCoordinate().getOrdinate(2));

        return snapshotOfLine2d;
    }

    public boolean isDroneLocationBelongsToTakeOff(final Point droneLocation, final Route route) {
        Point location = convert3dPointTo2dPoint(droneLocation);
        Polygon takeOffZone = (Polygon) (route.getCorridor().getGeometryN(0));
        Polygon takeOffZoneCut = getClosestToDroneCutOfCorridor(takeOffZone, droneLocation);
        return location.intersects(takeOffZoneCut);
    }

    public boolean isDroneLocationBelongsToLanding(final Point droneLocation, final Route route) {
        Point location = convert3dPointTo2dPoint(droneLocation);
        MultiPolygon corridor = route.getCorridor();
        Polygon landingZone = (Polygon) (corridor.getGeometryN(corridor.getNumGeometries() - 1));
        Polygon landingZoneCut = getClosestToDroneCutOfCorridor(landingZone, droneLocation);
        return location.intersects(landingZoneCut);
    }

}
