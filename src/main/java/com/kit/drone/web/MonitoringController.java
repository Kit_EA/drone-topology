package com.kit.drone.web;

import com.kit.drone.converter.DroneDataConverter;
import com.kit.drone.model.DroneData;
import com.kit.drone.resource.DroneDataResource;
import com.kit.drone.monitoring.MonitoringService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/monitoring")
@RequiredArgsConstructor
public class MonitoringController {

    private final MonitoringService monitoringService;
    private final DroneDataConverter droneDataConverter;

    @RequestMapping(value = "/drone/{droneId}", method = RequestMethod.POST)
    public void chooseDroneRoute(@PathVariable("droneId") String droneId,
                                 @RequestParam(value = "routeId") String routeId) {
        monitoringService.startMonitoring(droneId, routeId);
    }

    @RequestMapping(value = "/drone", method = RequestMethod.POST)
    public void sendCoordinates(@RequestBody DroneDataResource droneDataResource) {
        DroneData droneData = droneDataConverter.convert(droneDataResource);
        monitoringService.updateMonitoring(droneData);
    }

    @RequestMapping(value = "/drone/{droneId}", method = RequestMethod.DELETE)
    public void removeDroneRoute(@PathVariable("droneId") String droneId) {
        monitoringService.stopMonitoring(droneId);
    }
}
