package com.kit.drone.web;

import com.kit.drone.converter.RouteConverter;
import com.kit.drone.model.Route;
import com.kit.drone.repository.RouteRepository;
import com.kit.drone.resource.RouteResource;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/route")
@RequiredArgsConstructor
public class RouteController {

    private final RouteConverter routeConverter;
    private final RouteRepository routeRepository;

    @RequestMapping(method = RequestMethod.POST)
    public void addRoute(@RequestBody RouteResource routeResource) {
        Route route = routeConverter.convert(routeResource);
        routeRepository.save(route);
    }

    @RequestMapping(value = "/{routeId}", method = RequestMethod.DELETE)
    public void deleteRoute(@PathVariable long routeId) {
        routeRepository.deleteById(routeId);
    }

    @RequestMapping(value = "/{routeId}", method = RequestMethod.PUT)
    public void updateRoute(@RequestBody RouteResource routeResource, @PathVariable long routeId) {
        Route routeUpdate = routeConverter.convert(routeResource);

        Route routeUpdated = routeRepository.findById(routeId).get();
        routeUpdated.setName(routeUpdate.getName());
        routeUpdated.setPath(routeUpdate.getPath());
        routeUpdated.setCorridor(routeUpdate.getCorridor());

        routeRepository.save(routeUpdated);
    }

    @RequestMapping(value = "/{routeId}", method = RequestMethod.GET)
    public Route getRoute(@PathVariable long routeId) {
        Optional<Route> route = routeRepository.findById(routeId);
        return route.get();
    }

}
